/*
Michael Fong 
CS 418 MP4
12/5/14

Implementation of the Catmull-Clark Subdivision method
with the block I. Enjoy

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <GL/glut.h>

using namespace std;

float eye[3] = {0,0,4};

int nFPS = 30;

bool fillObj = false;				//draw object with GL_LINE or GL_FILL / show vertices or colors
bool moveEye = false;				//determines if the eye is moving
bool subdivide = false;
bool showMesh;						//display the mesh?

vector<GLfloat> origVert;				//holds the static vertex info from i.obj
vector<GLshort> origFace;				//holds quadruptles of vertices with each quadruptle making a face of the shape

//creating classes
class vertex;
class face;
class halfEdge;

//global mesh variables
std::vector<halfEdge*> glEdgeVect;
std::vector<vertex*> glVertVect;
std::vector<face*> glFaceVect;

//each vertice
class vertex{
public:
	vertex::vertex();
	double x, y, z;
	halfEdge * outEdge;
};

//face on the quad
class face{
public:
	face::face();
	//4 vertex
	vertex *vert[4];
	//one edge of the face
	halfEdge * oneEdge; 
	vertex * facePoint;
};

//according to the halfedge data structure
class halfEdge{
public:
	halfEdge::halfEdge();
	//start vertex
	vertex * start;
	//end vertex
	vertex * end; 
	halfEdge * next; 
	halfEdge * opposite;
	face * heFace; 

	vertex* edgePoint;
	halfEdge* half1;
	halfEdge* half2;

};

//constructors
halfEdge::halfEdge(){
	start = NULL;
	end = NULL;
	next = NULL;
	opposite = NULL;
	heFace = NULL;
	edgePoint = NULL;
	half1 = NULL;
	half2 = NULL;
}

face::face(){
	oneEdge = NULL;
	facePoint = NULL;
	for(int i = 0; i < 4; i++){
		vert[i] = NULL;
	}
}

vertex::vertex(){
	//one edge leaving the vertex
	outEdge = NULL;
	x = y = z = 0;

}

void timer(int v)
{
	static float t = 0;
	static float p0[3] = {0,0,4};
	static float p1[3] = {5,0,-5};
	static float p2[3] = {-5,0,-5};
	static float p3[3] = {0,0,4};
	//calculate bezier curve for eye
	if(moveEye){
		for(int i = 0; i < 3; i++){
			eye[i] = pow(1-t,3)*p0[i] + 3*pow(1-t,2)*t*p1[i] + 3*(1-t)*pow(t,2)*p2[i] + pow(t,3)*p3[i];
		}
		t+=.005;
		if(t>1)
			t = 0;
	}
	glutPostRedisplay(); // call display function
	glutTimerFunc(1000/nFPS,timer,v); // restart timer 

}
/*
makes a face point for every face
*/
void createFacePoints(std::vector<face*> &faceVect, std::vector<vertex*> &vertVect){
	std::vector<face*>::iterator it;
	vertex tVert[4];
	for(it = faceVect.begin(); it < faceVect.end(); it++){
	
		//load the verts of this face
		for(int i = 0; i < 4; i++){
			tVert[i] = *((*it)->vert[i]);
		}

		vertex * newFaceP = new vertex;

		//average of the four vertices on the face
		(*newFaceP).x = (tVert[0].x + tVert[1].x + tVert[2].x + tVert[3].x)/4;
		(*newFaceP).y = (tVert[0].y + tVert[1].y + tVert[2].y + tVert[3].y)/4;
		(*newFaceP).z = (tVert[0].z + tVert[1].z + tVert[2].z + tVert[3].z)/4;

		//storing in the global 
		vertVect.push_back(newFaceP);
		(*it)->facePoint = newFaceP;

	}

}

//makes a edge point for every edge
void createEdgePoints(std::vector<halfEdge*> &HEvect, std::vector<vertex*> &vertVect){
	std::vector<halfEdge*>::iterator it;

	for(it = HEvect.begin(); it < HEvect.end(); it++){
		if( (*it)->opposite->edgePoint != NULL){
			//use the edge point already calculated
			(*it)->edgePoint = (*it)->opposite->edgePoint;

		}else{
			//calculate the edge point based off of the average of the face points and the two vertices adjacent to the edge

			//faces
			vertex faceVert1 = *((*it)->heFace->facePoint);
			vertex faceVert2 = *((*it)->opposite->heFace->facePoint);
			//edges
			vertex edgeVert1 = *((*it)->end);
			vertex edgeVert2 = *((*it)->opposite->end);

			vertex * newEdgeVertex = new vertex;
			
			//averaging
			(*newEdgeVertex).x = (faceVert1.x + faceVert2.x + edgeVert1.x + edgeVert2.x)/4;
			(*newEdgeVertex).y = (faceVert1.y + faceVert2.y + edgeVert1.y + edgeVert2.y)/4;
			(*newEdgeVertex).z = (faceVert1.z + faceVert2.z + edgeVert1.z + edgeVert2.z)/4;

			//storing in global
			vertVect.push_back(newEdgeVertex);
			(*it)->edgePoint = newEdgeVertex;
		}
	}
}

//move all of the old vertex points based on all of the adjacent edge and face points already calculated
void correctVerts(std::vector<vertex*> & vertVect){
	std::vector<vertex*>::iterator it;
	int i;
	int divider,mult;
	halfEdge* outEdges[5];

	vertex edgePoints[5];

	vertex edgePointAvg;
	vertex facePoints[5];

	vertex facePointAvg;
	vertex * currVert;
	
	for(it = vertVect.begin(); it < vertVect.end(); it++){
		currVert = *it;
		//get all of the edge and face points
		for(i = 0; i < 5; i++){
			if(i == 0){
				outEdges[i] = currVert->outEdge;
			}else{
				outEdges[i] = outEdges[i-1]->next->next->next->opposite;
			}

			edgePoints[i] = *(outEdges[i]->edgePoint); 
			facePoints[i] = *(outEdges[i]->heFace->facePoint);

		}
		//we know there are always 4 faces, but we also want to average depending on how many edges there are (3-5)
		if(outEdges[0] == outEdges[3]){
			//average
			edgePointAvg.x = (edgePoints[0].x + edgePoints[1].x + edgePoints[2].x)/3;
			edgePointAvg.y = (edgePoints[0].y + edgePoints[1].y + edgePoints[2].y)/3;
			edgePointAvg.z = (edgePoints[0].z + edgePoints[1].z + edgePoints[2].z)/3;

			facePointAvg.x = (facePoints[0].x + facePoints[1].x + facePoints[2].x)/3;
			facePointAvg.y = (facePoints[0].y + facePoints[1].y + facePoints[2].y)/3;
			facePointAvg.z = (facePoints[0].z + facePoints[1].z + facePoints[2].z)/3;
			divider = 4;
			mult = 1;
		}
		else if(outEdges[0] == outEdges[4]){
			//average
			edgePointAvg.x = (edgePoints[0].x + edgePoints[1].x + edgePoints[2].x + edgePoints[3].x)/4;
			edgePointAvg.y = (edgePoints[0].y + edgePoints[1].y + edgePoints[2].y + edgePoints[3].y)/4;
			edgePointAvg.z = (edgePoints[0].z + edgePoints[1].z + edgePoints[2].z + edgePoints[3].z)/4;

			facePointAvg.x = (facePoints[0].x + facePoints[1].x + facePoints[2].x + facePoints[3].x)/4;
			facePointAvg.y = (facePoints[0].y + facePoints[1].y + facePoints[2].y + facePoints[3].y)/4;
			facePointAvg.z = (facePoints[0].z + facePoints[1].z + facePoints[2].z + facePoints[3].z)/4;
			divider = 4;
			mult = 1;
		}
		else{
			//average 
			edgePointAvg.x = (edgePoints[0].x + edgePoints[1].x + edgePoints[2].x + edgePoints[3].x + edgePoints[4].x)/5;
			edgePointAvg.y = (edgePoints[0].y + edgePoints[1].y + edgePoints[2].y + edgePoints[3].y + edgePoints[4].y)/5;
			edgePointAvg.z = (edgePoints[0].z + edgePoints[1].z + edgePoints[2].z + edgePoints[3].z + edgePoints[4].z)/5;

			facePointAvg.x = (facePoints[0].x + facePoints[1].x + facePoints[2].x + facePoints[3].x + facePoints[4].x)/5;
			facePointAvg.y = (facePoints[0].y + facePoints[1].y + facePoints[2].y + facePoints[3].y + facePoints[4].y)/5;
			facePointAvg.z = (facePoints[0].z + facePoints[1].z + facePoints[2].z + facePoints[3].z + facePoints[4].z)/5;
			divider = 5;
			mult = 2;
		}

		//change the vertice accordingly
		currVert->x = (mult*currVert->x +  2*edgePointAvg.x + facePointAvg.x)/divider; 
		currVert->y = (mult*currVert->y +  2*edgePointAvg.y + facePointAvg.y)/divider; 
		currVert->z = (mult*currVert->z +  2*edgePointAvg.z + facePointAvg.z)/divider; 

	}
}


/*
now that everything is moved, we need to connect and create the new halfedges and faces to subdivide correctly
*/
void populateMesh(std::vector<face*> &faceVect, std::vector<face*> &newFaceVect, std::vector<halfEdge*> &newEdgeVect){
	std::vector<face*>::iterator faceIt;
	int i, x;
	for(faceIt = faceVect.begin(); faceIt < faceVect.end(); faceIt ++){
		face currFace = **faceIt;

		//find edges around one face
		halfEdge* origEdge[4];
		for(i = 0; i < 4; i++){
			if(i == 0)
				origEdge[i] = currFace.oneEdge;
			else
				origEdge[i] = origEdge[i-1]->next;
		}

		halfEdge* outerEdge[8];
		halfEdge* inOutEdge[8];
		//initialize new half edges
		for(i = 0; i < 8; i++){
			outerEdge[i] = new halfEdge;
			inOutEdge[i] = new halfEdge;
		}
		//set those half edges based on the original edges that were twice as long
		for(i = 0; i < 4; i++){
			origEdge[i]->half1 = outerEdge[2*i];
			origEdge[i]->half2 = outerEdge[2*i+1];
		}

		//set all of the pointers to the correct place
		for(i = 0; i < 4; i++){

			//new vertices for edges
			outerEdge[2*i]->start = origEdge[i]->start;
			outerEdge[2*i]->end = origEdge[i]->edgePoint;
			outerEdge[2*i+1]->start = origEdge[i]->edgePoint;
			outerEdge[2*i+1]->end = origEdge[i]->end;

			if(origEdge[i]->opposite->half1 != NULL){
				//setting opposite pointers
				//inside to out
				outerEdge[2*i+1]->opposite = origEdge[i]->opposite->half1;
				outerEdge[2*i]->opposite = origEdge[i]->opposite->half2;
				//out to in
				origEdge[i]->opposite->half1->opposite = outerEdge[2*i+1];
				origEdge[i]->opposite->half2->opposite = outerEdge[2*i];
			}
			//vertice has one pointer
			origEdge[i]->start->outEdge = outerEdge[2*i];

			//setting inner cross edges accordingly
			inOutEdge[2*i]->start = origEdge[i]->edgePoint;
			inOutEdge[2*i]->end = currFace.facePoint;
			inOutEdge[2*i+1]->start = currFace.facePoint;
			inOutEdge[2*i+1]->end = origEdge[i]->edgePoint;
			//setting opposites of those edges
			inOutEdge[2*i]->opposite = inOutEdge[2*i+1];
			inOutEdge[2*i+1]->opposite = inOutEdge[2*i];

			//vertex has one edge coming out of it
			origEdge[i]->edgePoint->outEdge = inOutEdge[2*i];
		}		

		//set face vertex edge pointer
		currFace.facePoint->outEdge = inOutEdge[7];
		x = 3;

		face* newFaces[4];
		for(i = 0; i < 4; i++){
			//setting the next pointer
			outerEdge[2*i]->next = inOutEdge[2*i];
			inOutEdge[2*i]->next = inOutEdge[2*x+1];
			inOutEdge[2*x+1]->next = outerEdge[2*x+1]; 
			outerEdge[2*x+1]->next = outerEdge[2*i];
			
			//create new faces/connect all face pointers
			newFaces[i] = new face;

			//setting vertices and faces for the half edges
			newFaces[i]->vert[0] = outerEdge[2*i]->start;
			outerEdge[2*i]->heFace = newFaces[i];
			newFaces[i]->vert[1] = inOutEdge[2*i]->start;
			inOutEdge[2*i]->heFace = newFaces[i];
			newFaces[i]->vert[2] = inOutEdge[2*x+1]->start;
			inOutEdge[2*x+1]->heFace = newFaces[i];
			newFaces[i]->vert[3] = outerEdge[2*x+1]->start;
			outerEdge[2*x+1]->heFace = newFaces[i];

			newFaces[i]->oneEdge = outerEdge[2*x+1];

			//counter
			if(i == 0){
				x = 0;
			}else{
				x ++;
			}
			
		}


		for(i = 0; i < 4; i++){
			//add face to face vector
			newFaceVect.push_back(newFaces[i]);

			//add edges to edge vector
			newEdgeVect.push_back(outerEdge[2*i]);
			newEdgeVect.push_back(outerEdge[2*i+1]);

			newEdgeVect.push_back(inOutEdge[2*i]);
			newEdgeVect.push_back(inOutEdge[2*i+1]);
		}
	}
}

/* t
this is the main caller of all the functions to do catmull clark subdivision
*/
void catmullClark(){

	std::vector<face*> nFaceVect;
	std::vector<halfEdge*> nEdgeVect;
	std::vector<vertex*> oldVertVect;

	halfEdge * tempEdge;
	face * tempFace;

	oldVertVect = glVertVect;

	//1. loop through faces 2. loop through edges 3. loop through points 4. connect everything
	createFacePoints(glFaceVect, glVertVect);
	createEdgePoints(glEdgeVect, glVertVect);
	correctVerts(oldVertVect);
	populateMesh(glFaceVect, nFaceVect, nEdgeVect); 

	//delete the past globals
	while(!glFaceVect.empty()){
		tempFace = glFaceVect.back();
		glFaceVect.pop_back();
		delete tempFace;
	}

	while(!glEdgeVect.empty()){
		tempEdge = glEdgeVect.back();
		glEdgeVect.pop_back();
		delete tempEdge;
	}
	//replace old mesh with new mesh
	glFaceVect = nFaceVect;
	glEdgeVect = nEdgeVect;
}


//populates the information of each face
void createFace(vertex * v1, vertex* v2, vertex * v3, vertex * v4, 
				std::vector<face*> &faceVect, 
				std::vector<halfEdge*> &edgeVect){
	
	face * nextFace = new face;
	int i;
	vertex * newV[4];
	
	newV[0] = v1;
	newV[1] = v2;
	newV[2] = v3;
	newV[3] = v4;
	
	halfEdge* newEdges[4];
	for(i = 0; i < 4; i++){
		newEdges[i] = new halfEdge;
	}
	for(i = 0; i < 4; i++){
		nextFace->vert[i] = newV[i];
		newEdges[i]->start = newV[i];
		newEdges[i]->end = newV[(i+1)%4];
		newV[i]->outEdge = newEdges[i];
		newEdges[i]->next = newEdges[(i+1)%4];
		newEdges[i]->heFace = nextFace;
	}
	nextFace->oneEdge = newEdges[3];

	faceVect.push_back(nextFace);

	for(i = 0; i < 4; i++){
		edgeVect.push_back(newEdges[i]);
	}
	
	

}

//resets the current mesh to go back to the original block i
void flushAndReset(std::vector<face*> &faceVect, std::vector<halfEdge*> &edgeVect, std::vector<vertex*> &vertVect){
	std::vector<face*>::iterator faceIt;
	std::vector<halfEdge*>::iterator edgeIt;
	vertex * tempVert;
	halfEdge * tempEdge;
	face * tempFace;
	int i = 0;
	//deleting old
	while(!faceVect.empty()){
		tempFace = faceVect.back();
		faceVect.pop_back();
		delete tempFace;
	}
	while(!edgeVect.empty()){
		tempEdge = edgeVect.back();
		edgeVect.pop_back();
		delete tempEdge;
	}
	while(!vertVect.empty()){
		tempVert = vertVect.back();
		vertVect.pop_back();
		delete tempVert;
	}

	//adding information from i.obj

	//creating vertices
    vertex* newVertex[32];
    for(i = 0; i < 32; i++){
    	newVertex[i] = new vertex;
    	vertVect.push_back(newVertex[i]);
    }
	for(i = 0; i < 32; i++){
		newVertex[i]->x = (double)origVert[3*i];
		newVertex[i]->y = (double)origVert[3*i+1];
		newVertex[i]->z = (double)origVert[3*i+2];
	}

	//creating faces
	for(i = 0; i < 30; i++){
		createFace(newVertex[origFace[4*i]], newVertex[origFace[4*i+1]], newVertex[origFace[4*i+2]], newVertex[origFace[4*i+3]], faceVect, edgeVect);
	}
	

	//finds the edges that are next to each other and correctly assigns the opposite pointers
	std::vector<halfEdge*>::iterator edgeIt1;
	std::vector<halfEdge*>::iterator edgeIt2;
	
	for( edgeIt1 = edgeVect.begin(); edgeIt1 < edgeVect.end(); edgeIt1 ++){
		for(edgeIt2 = edgeIt1 +1; edgeIt2 < edgeVect.end(); edgeIt2++){
			if(((*edgeIt1)->start == (*edgeIt2)->end) &&((*edgeIt1)->end == (*edgeIt2)->start)){
				(*edgeIt1)->opposite = *edgeIt2;
				(*edgeIt2)->opposite = *edgeIt1;			
			}
		}
	}
}

//inserts into our static array of faces from the i.obj
void insertIntoFace(string line){
	int end = 1;
	int begin, i;
	string temp;
	for(i = 0; i < 4; i++){
		begin = end+1;
		if(i < 3)
			end = line.find(" ",begin);
		else
			end = line.size();
		
		temp = line.substr(begin, end - begin);
		origFace.push_back(atoi(temp.c_str()));
	}
}
//inserts into our static array of vertcies from the i.obj
void insertIntoVert(string line){
	int end = 1;
	int begin,i;
	string temp;
	for(i = 0; i < 3; i++){
		begin = end+1;
		if(i < 3)
			end = line.find(" ",begin);
		else
			end = line.size();
		temp = line.substr(begin, end - begin);;
		//convert to float
		origVert.push_back((float) atof(temp.c_str()));

	}
}

//reads the i.obj file 
void readFile(char* filename){
	ifstream object(filename);
	string line;
	if (!object.is_open())
	{
		cout << "Unable to open file"; 
		return;
	}
	//get the file line by line and process it
	while ( object.good() )
	{
		getline (object,line);
		if(line.size() < 1)
			continue;
		else if(line[0] == 'v')
			insertIntoVert(line);
		else if(line[0] == 'f')
			insertIntoFace(line);
	}
	object.close();

}

//initializes everything and creates the 3d blockI
void init(void)
{
	// init data, setup OpenGL environment here
	glClearColor(0.0,0.0,0.0,0.0);	//background color is black
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPointSize(14.0);
	glEnable(GL_DEPTH_TEST);
	readFile("i.obj");
	flushAndReset(glFaceVect, glEdgeVect, glVertVect);
}

void keyboard(unsigned char key, int x, int y)
{

	switch (key) 
	{
		case 27:
			// ESC hit, so quit
			cout<<"demonstration finished"<<endl;
			exit(0);	
		case'f':
			fillObj = !fillObj;
			if(fillObj)
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			break;
		//toggle the movement of the eye
		case 'e':
			moveEye = !moveEye;
			if(moveEye)
				cout<<"eye movement on"<<endl;
			else
				cout<<"eye movement off"<<endl;
			break;
		//reset
		case'r':
			flushAndReset(glFaceVect, glEdgeVect, glVertVect);
			break;
		//subdivide once
		case 's':
			subdivide = true;
			break;
	}
}

//displays everything
void display(void)
{

	//					0		1		2		3		4		5		6		7	
	float colors[] = {1,0,0,  0,1,0,  0,0,1,  0,1,0,  0,1,1,  1,0,1,  1,1,1,  0,1,0,  
	//					8		9		10		11		12		13		14		15
						1,0,0,  0,1,0,  1,0,0,  0,1,0,  0,1,0,  1,1,0,  0,0,1,  1,0,0,
	//					16		17		18		19		20		21		22		23
						1,0,0,  0,0,1,  0,1,0,  0,0,1,  1,0,0,  0,1,1,  0,1,0,  0,1,0,
	//					24		25		26		27		28		29		30		31
						1,0,0,  0,1,0,  0,1,0,  0,1,0,  1,0,0,  0,0,1,  1,0,0,  0,1,0};

	// put your OpenGL display commands here
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	// reset OpenGL transformation matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity(); // reset transformation matrix to identity

	// setup look at transformation so that 
	// eye is at : (0,0,3) look at : (0,0,0)
	// up direction is +y axis
	gluLookAt(eye[0], eye[1], eye[2],  0.0,-.5,0.0,  0.0,1.0,0.0);

	//if set to true subdivide once
	if(subdivide){
		catmullClark();
		subdivide = false;
	}

	//draws everything quad by quad with a color in the array above
	int i,x=0;
	std::vector<face*>::iterator finalFaces;
	face* tempF;
	for(finalFaces = glFaceVect.begin(); finalFaces < glFaceVect.end(); finalFaces++){
		tempF = *finalFaces;

		glBegin(GL_QUADS);
			for(i = 0; i < 4; i++){

				glColor3f(colors[x],colors[x+1],colors[x+1]);

				glVertex3f(tempF->vert[i]->x, tempF->vert[i]->y, tempF->vert[i]->z);
				x+=3;
				x = x %31;
			}
		glEnd();
	}

	glutSwapBuffers();	// swap front/back framebuffer to avoid flickering 
	glFlush();
	glutPostRedisplay();
}
void reshape (int w, int h)
{
	// reset viewport ( drawing screen ) size
	glViewport(0, 0, w, h);
	float fAspect = ((float)w)/h;
	// reset OpenGL projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70.f,fAspect,0.001f,30.f); 
}
int main(int argc, char* argv[])
{

	glutInit(&argc, (char**)argv);
	// set up for double-buffering & RGB color buffer & depth test
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); 
	glutInitWindowSize (500, 500); 
	glutInitWindowPosition (100, 100);
	glutCreateWindow ((const char*)"MP4");

	init(); // setting up user data & OpenGL environment
	subdivide = false;
	// set up the call-back functions 
	glutDisplayFunc(display);  // called when drawing 
	glutReshapeFunc(reshape);  // called when change window size
	glutKeyboardFunc(keyboard); // called when received keyboard interaction
	glutTimerFunc(100,timer,nFPS); // a periodic timer. used for updating animation
	
	glutMainLoop(); // start the main message-callback loop

	return 0;
}
