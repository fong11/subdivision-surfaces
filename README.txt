CS 418 MP4 README
12/5/14

by:
Michael Fong
fong11

To open the project you'll need Microsoft Visual Studio 2013. 
Open the mp4.sln file within the Source folder to run the project. 

Controls:
e : change the camera to go on a Bezier curve
s : subdivide once
f : switch from FILL or LINE to see the mesh vs the colors
r : reset to original position and without subdivision
ESC : quit

Youtube:
http://youtu.be/93NIgoaVo_0